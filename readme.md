# Challenge

I (Albert Hendriks) created this program as a challenge to beat me. 

1. Is the output of this Java program deterministic or nondeterministic?
2. If it's deterministic, what is its behavior?
3. If it's nondeterministic, will there be a similar pattern between different runs?

Anyone who can correctly predict and explain the answers without actually running the program, has more experience with
Java concurrency than I have.
