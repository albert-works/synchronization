package com.albertworks;

/**
 * I (Albert Hendriks) created this program as a challenge to beat me. It was tested with Java 11.
 *
 * Is the output of this Java program deterministic or nondeterministic?
 * If it's deterministic, what is its behavior?
 * If it's nondeterministic, will there be a similar pattern between different runs?
 *
 * Anyone who can correctly predict and explain the answers without actually running the program, has more experience
 * with Java concurrency than I have.
 */
public class Application {

    private long i = 0, k = 0;
    private Object lock = new Object();

    public static void main(String args[]) {
        new Application().kickoff();
    }

    private void kickoff() {
        new Thread(() -> {
            while (true) {
                if (k <= i) {
                    k++;
                }
            }
        }).start();

        while (true) {
            synchronized (lock) {
                if (i <= k) {
                    i++;
                }
                if (i < k) {
                    System.out.println("It happened");
                }
            }
        }
    }
}
